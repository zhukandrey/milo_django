### Installation:
1.
```
git clone https://github.com/zhukandrey/milo_django
cd milo_django
```
Optional:
```
virtualenv <path to environment>
source <path to environment>/bin/activate
```
2.
```
pip install -r requirements.txt
cd milo_django
python manage.py migrate
python manage.py runserver
```

### Release Notes:
- [x] Extended User model
- [x] Created views
- [x] Created template tags
- [x] Implemented columns on list view with template tags
- [x] Unit tests
- [x] Download CSV link on the list view
- [ ]  Improve Unit test coverage


### Screenshot
![List view](https://github.com/zhukandrey/milo_django/blob/master/screenshots/screenshot-list.png)
