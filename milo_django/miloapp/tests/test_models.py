from django.test import TestCase
from miloapp.models import CustomUser as User


class UserTestCase(TestCase):

    def setUp(self):
        User.objects.create(username='john', email='john@gmail.com', password='john123')
        User.objects.create(username='david', email='david@gmail.com', password='david123')

    def test_users_have_correct_random_number(self):

        john = User.objects.get(username='john')
        david = User.objects.get(username='david')

        self.assertIn(john.random_number, range(101))
        self.assertIn(david.random_number, range(101))
