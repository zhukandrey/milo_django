from django.test import TestCase, RequestFactory
from miloapp.views import all_users_list, view_user
from django.db.models.query import QuerySet
from miloapp.models import CustomUser as User


class MiloappBaseTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user1 = User.objects.create(
            username='john',
            email='john@gmail.com',
            password='john123'
        )
        cls.user2 = User.objects.create(
            username='david',
            email='david@gmail.com',
            password='david123'
        )


class IndexViewTestCase(MiloappBaseTestCase):

    def test_list_view_basic(self):
        """
        Test that list view returns a 200 response and uses the correct template
        """
        request = self.factory.get('/')
        with self.assertTemplateUsed('miloapp/list.html'):
            response = all_users_list(request)
            self.assertEqual(response.status_code, 200)

    def test_list_view_return_users(self):
        """
        Test that the list view will attempt to return users
        """
        response = self.client.get('/')

        users = response.context['users']

        self.assertIs(type(users), QuerySet)
        self.assertEqual(len(users), 2)
        self.assertEqual(users[0].username, 'john')