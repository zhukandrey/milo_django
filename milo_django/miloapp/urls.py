from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^user/(?P<username>[\w\-]+)/$', views.view_user, name='view'),
    url(r'^edit/(?P<username>[\w\-]+)/$', views.edit_user, name='edit'),
    url(r'^delete/(?P<username>[\w\-]+)/$', views.delete_user, name='delete'),
    url(r'^add/$', views.add_user, name='add'),
    url(r'^download/$', views.download_list, name='download'),
    url(r'^$', views.all_users_list, name='index'),
]