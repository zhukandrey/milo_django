from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.timezone import now
from random import randint
from django.core.validators import MinValueValidator, MaxValueValidator


class CustomUser(AbstractUser):
    date_of_birth = models.DateField(default=now)
    random_number = models.IntegerField(default=randint(1, 100),
                                        validators=[
                                            MinValueValidator(1),
                                            MaxValueValidator(100)
                                        ])
