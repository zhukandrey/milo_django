from django import template
from django.utils.timezone import now
from dateutil.relativedelta import relativedelta

register = template.Library()


@register.simple_tag
def eligible(date_of_birth):
    if date_of_birth > now().date() - relativedelta(years=13):
        return 'blocked'
    else:
        return 'allowed'


@register.simple_tag
def bizz_fuzz(random_number):
    message = ''
    if random_number % 3 == 0:
        message += 'Fizz'
    if random_number % 5 == 0:
        message += 'Buzz'

    return message or random_number
