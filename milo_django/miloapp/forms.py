from django.forms import ModelForm
from .models import CustomUser


class AddUserForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'password', 'date_of_birth']


class EditUserForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'date_of_birth', 'random_number']
