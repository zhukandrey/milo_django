from django.shortcuts import render, redirect, get_object_or_404
from .models import CustomUser as User
from .forms import EditUserForm, AddUserForm
from django.http import HttpResponse
import csv
from .templatetags import milo_tags


def all_users_list(request):
    all_users = User.objects.all()

    return render(request, 'miloapp/list.html', {'users': all_users})


def view_user(request, username):
    user = User.objects.get(username=username)

    return render(request, 'miloapp/view.html', {'user': user})


def edit_user(request, username):
    user = get_object_or_404(User, username=username)

    if request.method == 'POST':
        form = EditUserForm(request.POST or None, instance=user)
        if form.is_valid():
            form.save()
        return redirect(all_users_list)
    else:
        form = EditUserForm(instance=user)

    return render(request, 'miloapp/edit.html', {'form': form, 'user': user})


def add_user(request):
    if request.method == 'POST':
        form = AddUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(all_users_list)
    else:
        form = AddUserForm()

    return render(request, 'miloapp/add.html', {'form': form})


def delete_user(request, username):
    user = get_object_or_404(User, username=username)
    user.delete()

    return redirect(all_users_list)


def download_list(request):
    users = User.objects.all()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="list_of_users.csv"'

    writer = csv.writer(response)
    writer.writerow(['Username', 'Birthday', 'Eligible', 'Random Number', 'BizzFazz'])
    writer.writerows([[user.username, user.date_of_birth, milo_tags.eligible(user.date_of_birth),
                       user.random_number, milo_tags.bizz_fuzz(user.random_number)]
                      for user in users])

    return response
